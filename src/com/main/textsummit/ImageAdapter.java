package com.main.textsummit;

import com.main.textsummit.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageAdapter extends BaseAdapter {
	private Context context;
	private final String[] icon_text;
 
	public ImageAdapter(Context context, String[] iconValues) {
		this.context = context;
		this.icon_text = iconValues;
	}
 
	public View getView(int position, View convertView, ViewGroup parent) {
 
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
		View gridView;
 
		if (convertView == null) {
 
			gridView = new View(context);
 
			// get layout from mobile.xml
			gridView = inflater.inflate(R.layout.grid_item, null);
 
			// set value into textview
			TextView textView = (TextView) gridView
					.findViewById(R.id.grid_item_label);
			textView.setText(icon_text[position]);
 
			// set image based on selected text
			ImageView imageView = (ImageView) gridView
					.findViewById(R.id.grid_item_image);
 
			String icon = icon_text[position];
 
			if (icon.equals("TextSummIt")) {
				imageView.setImageResource(R.drawable.textsummit);
			} else if (icon.equals("Favorites")) {
				imageView.setImageResource(R.drawable.doc_icon);
			} else if (icon.equals("Settings")) {
				imageView.setImageResource(R.drawable.settings_icon);
			} else {
				imageView.setImageResource(R.drawable.help_icon);
			}
 
		} else {
			gridView = (View) convertView;
		}
 
		return gridView;
	}
 
	public int getCount() {
		return icon_text.length;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}
 
}
