package com.main.textsummit;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedSet;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.swing.text.html.HTML;

import net.didion.jwnl.JWNL;
import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.data.POS;
import net.didion.jwnl.dictionary.*;
import opennlp.tools.cmdline.postag.POSModelLoader;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;

import org.apache.commons.logging.LogFactory;
import org.w3c.dom.html.HTMLDListElement;

import com.aliasi.lm.TokenizedLM;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;
import com.aliasi.tokenizer.TokenizerFactory;
import com.aliasi.util.Files;
import com.aliasi.util.ScoredObject;

import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.IRAMDictionary;
import edu.mit.jwi.RAMDictionary;
import edu.mit.jwi.data.ILoadPolicy;
import edu.mit.jwi.morph.WordnetStemmer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.text.Spanned;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TableRow.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class SummarizeTextActivity extends Activity {

	public static final String INTENT_ACTION_SUMMARIZE_TEXT = "com.android.main.SUMMARIZE_TEXT_ACTION";
	private static final String LOGTAG = "Summarize:";
	// private final static org.apache.commons.logging.Log LOGTR = LogFactory
	// .getLog(TextRankScore.class.getName());

	private int wordCount;
	private ArrayList<String> fileWordList;
	private ArrayList<String> stopWordList;
	private ArrayList<String> taggedWordList;
	private ArrayList<String> sentenceList;
	private ArrayList<String> refList;
	private ArrayList<String> keyWordList = new ArrayList<String>();
	TreeMap<Object, Integer> keywordMap = new TreeMap<Object, Integer>();

	Sentence2[] mySentence;
	Sentence2[] newSentence;

	// Collocations
	private static int NGRAM = 3;
	private static int MIN_COUNT = 0;
	private static int MAX_NGRAM_REPORTING_LENGTH = 2;
	private static int NGRAM_REPORTING_LENGTH = 2;
	private static int MAX_COUNT = 100;

	// WordNet
	private HashMap<String, String> AllWords = null;

	// Layout Object
	TextView topic;
	TextView kwList;
	String filename, refFilename;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.summlayout);
		System.out.println("########################### onCreate ############");
		AlertDialog alertDialog = new AlertDialog.Builder(
				SummarizeTextActivity.this).create();
		Bundle extras = getIntent().getExtras();

		filename = extras.getString("SelectedFilePath");
		refFilename = extras.getString("SelectedRefFilePath");

		topic = (TextView) findViewById(R.id.topic);
		kwList = (TextView) findViewById(R.id.keyWordList);

        System.out.println("########################### Object created ############");

		// Set action for this activity
		Intent thisInt = this.getIntent();

		System.out.println("########################### Check textfiles ############");
		if (thisInt.getAction().equalsIgnoreCase(INTENT_ACTION_SUMMARIZE_TEXT)) {
			Log.d(LOGTAG, "Action: Summarize Text");

			if (!filename.endsWith(".txt") || !refFilename.endsWith(".txt")) {
				alertDialog.setTitle("Error Message");
				alertDialog.setMessage("Please insert a text file.");

				alertDialog.setButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// return to previous activity
								finish();
							}
						});
				alertDialog.show();
			} else
				try {
					System.out.println("########################### Generate summary ############");
					System.out.println(filename + " " + refFilename);
					generateSummary(filename, refFilename);
				} catch (Exception e) {
					e.printStackTrace();
				} catch (Throwable e) {
					e.printStackTrace();
				}
		}

	}

	/***********************************************************************
	 * Android menu settings
	 * 
	 ***********************************************************************/
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// MenuInflater menuInflater = getMenuInflater();
		getMenuInflater().inflate(R.menu.menusummary, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_save:
			return true;

		case R.id.menu_new:
			Intent i = new Intent(SummarizeTextActivity.this,
					TextSummItActivity.class);
			startActivity(i);
			return true;

		case R.id.menu_home:
			return true;

		case R.id.menu_stats:
			return true;

		case R.id.menu_exit:
			finish();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onBackPressed() {
		// do nothing
	}

	/***********************************************************************
	 * Start generating summary
	 * 
	 * @param selected_filename
	 *            , selected_refFile
	 * @throws Throwable 
	 ***********************************************************************/
	public void generateSummary(String selected_filename,
			String selected_refFile) throws Throwable {

		try {

			/****************************************************************
			 * Step 1: Pre-processing
			 ****************************************************************/
			// Read file input for tokenizing and word storing
			System.out.println("Reference filename: " + selected_refFile);

			BufferedReader br = new BufferedReader(new FileReader(
					selected_filename));
			StreamTokenizer tokens_fileWords = new StreamTokenizer(br);

			// Read file input for input summary storing
			FileInputStream flStream = new FileInputStream(selected_filename);
			FileChannel fc = flStream.getChannel();
			ByteBuffer bb = ByteBuffer.allocate((int) fc.size());
			fc.read(bb);
			fc.close();
			flStream.close();

			String content = new String(bb.array());
			String sentenceDelim = ".?!"; // specify if a sentence is found
			StringTokenizer sntncs = new StringTokenizer(content, sentenceDelim);

			// Read file input for reference summary storing
			FileInputStream referenceFile = new FileInputStream(
					selected_refFile);
			FileChannel referenceFC = referenceFile.getChannel();
			ByteBuffer referenceBB = ByteBuffer.allocate((int) referenceFC
					.size());

			referenceFC.read(referenceBB);
			referenceFC.close();
			referenceFile.close();

			String refContent = new String(referenceBB.array());
			StringTokenizer refToken = new StringTokenizer(refContent,
					sentenceDelim);

			// ********** TOKENIZATION ********* //
			storeSentence(sntncs);
			// storeRefSentence(refToken);
			storeWords(tokens_fileWords);

			System.out.println("########################### tokenized and stored ############");
			// *********** POS-TAG ************* //
//			postagWords();

			// ********* COLLOCATIONS ********** //
			// collocations(selected_filename);

			// ****** STOP WORDS REMOVAL ******* //
			removeStopWords();
			System.out.println("########################### removed stopwords ############");
			
			// *********** STEM WORDS ********** //
			stemWords();
			System.out.println("########################### stemmed words ############");
			
			// for (int i = 0; i < fileWordList.size(); i++) {
			// stem(fileWordList.get(i).toString());
			// }

			System.out.println("########################### Preprocessing done ############");
			// ************** KEYWORDS EXTRACTION ************** //
			TreeMap<String, Integer> mapKey = new TreeMap<String, Integer>();
			for (int i = 0; i < fileWordList.size(); i++) {
				String myWord = fileWordList.get(i);
				Integer freq2 = mapKey.get(myWord);
				mapKey.put(myWord, (freq2 == null) ? 1 : freq2 + 1);
			}
			Word[] addKeWords = new Word[mapKey.size()];
			Set<?> keySet = mapKey.entrySet();
			Iterator<?> iteratorkey = keySet.iterator();

			int gg = 0;

			while (iteratorkey.hasNext()) {
				addKeWords[gg] = new Word();
				Map.Entry mapEntry = (Map.Entry) iteratorkey.next();
				String newWord = (String) mapEntry.getKey();
				int newFreq = (Integer) mapEntry.getValue();
				addKeWords[gg].setName(newWord);
				addKeWords[gg].setFreq(newFreq);
				gg++;
			}

			// Extract only 20% of the top keywords
			keyWordList = new KeyWordProcess().addKeyWordsToFile(addKeWords,
					wordCount, 0.2);

			// ************ END KEYWORDS EXTRACTION ************* //
			System.out.println("########################### keywords extraction done ############");

			/*******************************************************************
			 * Features weight calculation
			 *******************************************************************/
			System.out.println("########################### Features weight calc ############");
			TreeMap<String, Integer> mapSen = new TreeMap<String, Integer>();
			for (int i = 0; i < sentenceList.size(); i++) {
				mapSen.put(sentenceList.get(i).toString(), i);
			}

			mySentence = new Sentence2[sentenceList.size()];
			newSentence = new Sentence2[sentenceList.size()];

			for (int i = 0; i < sentenceList.size(); i++) {
				mySentence[i] = new Sentence2();

				String newSent = (String) sentenceList.get(i);
				String newTitle = (String) sentenceList.get(0);
				String newFirst = (String) sentenceList.get(1);
				String newLast = (String) sentenceList
						.get(sentenceList.size() - 1);

				// mySentence[i].setEntrance(i + 1);
				// mySentence[i].setSentenceKeywords(words_freq,
				// wordMap.size()); //keywords are determined by TextRank Score
				mySentence[i].setSelectedFile(selected_filename);
				mySentence[i].setTitle(newTitle);
				mySentence[i].setFirstSentence(newFirst);
				mySentence[i].setLastSentence(newLast);
				mySentence[i].setSentence(newSent);
				mySentence[i].setSentenceWords();

				mySentence[i].compareWithTitle();
				mySentence[i].compareWithFirst();
				mySentence[i].compareWithLast();
				mySentence[i].averageSFL();
				mySentence[i].secPosModel(mapSen, sentenceList.size());
//				mySentence[i].wordNet(keyWordList);

				// Sorting the sentence score
				newSentence[i] = new Sentence2();
				mySentence[i].totalSentenceScore();
				newSentence[i] = mySentence[i];
			}

			// Sentence not tokenize properly e.g. 2p.m. 6a.m. U.S.
			System.out.println("########################### Sort sentence done ############");

			// Sort sentences according to feature weights
			for (int i = 0; i < newSentence.length; i++) {
				for (int j = i + 1; j < newSentence.length; j++) {
					if (newSentence[i].totalSentenceScore() < newSentence[j].totalSentenceScore()) {
						Sentence2 tmp = newSentence[i];
						newSentence[i] = newSentence[j];
						newSentence[j] = tmp;
					}
//					System.out.println(newSentence[j].returnSentence().toString());
				}

			}

			double sLength = 20;
			int sL;
			double sLl;

			sLl = sLength / 100 * sentenceList.size();
			sL = (int) Math.ceil(sLl);

			String[] summarized;
			summarized = new String[sL];

			for (int i = 0; i < sL; i++) {
				summarized[i] = (String) newSentence[i].returnSentence();
				// System.out.println("" + summarized[i]);
			}

			System.out.println("########################### summarized done ############");

			/********************************************************************
			 * Evaluation Process
			 ********************************************************************/
			System.out
					.println("########################### Evaluation ############");

			// store reference summary sentence
			int numOfRefSen = storeRefSentence(refToken);

			System.out.println(refList.toString());

			// strings for summarized summary for LCS input purpose
			String[] summString;
			summString = new String[numOfRefSen];

			int z = 0;
			for (z = 0; z < numOfRefSen; z++) {
				summString[z] = refList.get(z);
				// System.out.println(""+summString[z]);
			}

			// Counting words for summarized output
			double summCount = 0;
			for (int c = 0; c < sL; c++) {
				String[] split_summ = summarized[c].split("\\s");

				for (int ii = 0; ii < split_summ.length; ii++) {
					summCount++;
				}
			}

			// Counting words for referenced summary
			double sCount = 0;
			for (int c = 0; c < numOfRefSen; c++) {
				String[] split_ref = summString[c].split("\\s");

				for (int ii = 0; ii < split_ref.length; ii++) {
					sCount++;
				}
			}

			// Evaluate using LCS
			int nGram = 0;
			int tmpNGram = 0;
			String lcs_str = "";

			for (int c = 0; c < sL; c++) {
				for (int i = 0; i < numOfRefSen; i++) {
					LCS lcs = new LCS(summarized[c], summString[i]);
					lcs_str = lcs.getLcs();
					String[] splitS = lcs_str.split(" ");
					for (int j = 0; j < splitS.length; j++) {
						if (splitS[j].equals(" ")) {
						} else {
							tmpNGram++;
						}
					}
					// taking the longest LCS
					if (tmpNGram > nGram)
						nGram = tmpNGram;
				}
			}

			System.out.println("" + nGram);
			double recall = 0;
			double precision = 0;

			recall = nGram / sCount;
			precision = nGram / summCount;

			double beta = precision / recall;

			double fScore = 0;

			// counting the final f-score for system summarized output
			fScore = ((1 + Math.pow(beta, 2)) * recall * precision)
					/ (recall + Math.pow(beta, 2) * precision);

			System.out.println("nGram: " + nGram + "\n"
					+ "Total summary length: " + summCount + "\n"
					+ "Total reference length: " + sCount + "\n" + "Recall: "
					+ recall + "\n" + "Precision: " + precision + "\n"
					+ "Fscore: " + fScore);

			/**********************************************************************
			 * Set the text results
			 **********************************************************************/
			topic.setText(sentenceList.get(0));
			// kwList.setMovementMethod(new ScrollingMovementMethod());
			kwList.setText(keyWordList.toString());

			for(int i=1; i<=summarized.length; i++){
				TableLayout tl = (TableLayout)findViewById(R.id.contentLayout);
				
				TableRow row = new TableRow(this);
		        row.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
		        
		        /***** Create a textview to be the row-content. *****/
		        TextView tvCol = new TextView(this);
		        tvCol.setText("�  ");
		        tvCol.setLayoutParams(new LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		        
		        TextView tvCol2 = new TextView(this);
		        tvCol2.setText(summarized[i].toString() + ". ");
		        tvCol2.setLayoutParams(new LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		        
		        /****** Add Textview to row. ******/
		        row.addView(tvCol);
		        row.addView(tvCol2);
		        
		        ////// skip a line //////
		        TableRow row2 = new TableRow(this);
		        row2.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
		        
		        TextView tvRow = new TextView(this);
		        row2.addView(tvRow);
		        
		        /****** Add row to TableLayout. *******/
		        tl.addView(row);
		        tl.addView(row2);
			}
			// sen1.setMovementMethod(new ScrollingMovementMethod());
//			sen1.setText(summarized[1].toString() + ". ");
//			sen2.setText(summarized[2].toString() + ". ");
//			sen3.setText(summarized[3].toString() + ". ");

		} catch (IOException e) {
			e.getStackTrace();
		}
	}

	/*******************************************************************
	 * Tokenize document into sentences and store into an ArrayList
	 * 
	 * @param sntncs
	 *******************************************************************/
	public void storeSentence(StringTokenizer sntncs) {

		sentenceList = new ArrayList<String>();

		int numOfSen = 0;

		while (sntncs.hasMoreTokens()) {
			String mySentence = sntncs.nextToken();
			mySentence = mySentence.trim();
			sentenceList.add(mySentence);
			numOfSen++;
		}
	}

	/*******************************************************************
	 * Tokenize reference summary into sentences and store into an ArrayList
	 * 
	 * @param refToken
	 *******************************************************************/
	public int storeRefSentence(StringTokenizer refToken) {
		refList = new ArrayList<String>();

		int numOfRefSen = 0;
		while (refToken.hasMoreTokens()) {
			String mySentence = refToken.nextToken();
			// deleting spaces in front and at the back of each sentences
			mySentence = mySentence.trim();
			refList.add(mySentence);
			numOfRefSen++;
		}
		return numOfRefSen;
	}

	/*********************************************************************
	 * Tokenize sentences into words and store in a an ArrayList
	 * 
	 * @param tokens_fw
	 * @throws IOException
	 *********************************************************************/
	public void storeWords(StreamTokenizer tokens_fw) throws IOException {
		fileWordList = new ArrayList<String>();
		String fileWord = "";
		char fileMychar;

		tokens_fw.ordinaryChar(39); // to treat aposthrophe as an ordinary
									// character
		tokens_fw.ordinaryChar('/'); // to treat slash as an ordinary character
		tokens_fw.nextToken();

		while (tokens_fw.ttype != StreamTokenizer.TT_EOF) {
			if (tokens_fw.ttype == StreamTokenizer.TT_WORD) {
				fileWord = tokens_fw.sval;
				fileMychar = fileWord.charAt(fileWord.length() - 1);

				if ('.' == fileMychar) {
					fileWord = fileWord.substring(0, fileWord.length() - 1);
				}

				wordCount++;
				fileWord = fileWord.toLowerCase();
				fileWordList.add(fileWord);
			}
			tokens_fw.nextToken();
		}
	}

	/**************************************************************************
	 * Remove all stop words which are unimportant
	 * 
	 * @throws IOException
	 **************************************************************************/
	public void removeStopWords() throws IOException {

		AssetManager assetManager = getAssets();

		stopWordList = new ArrayList<String>();

		String stoplistWord = "";
		char stoplistMychar;

		// To load text file
		InputStream stopWord;
		try {

			stopWord = assetManager.open("stopList.txt");

			InputStreamReader stopWord_read = new InputStreamReader(stopWord);
			StreamTokenizer tokens_sw = new StreamTokenizer(stopWord_read);

			// tokens_sw.nextToken();
			while (tokens_sw.ttype != StreamTokenizer.TT_EOF) {
				if (tokens_sw.ttype == StreamTokenizer.TT_WORD) {
					stoplistWord = tokens_sw.sval;
					stoplistMychar = stoplistWord
							.charAt(stoplistWord.length() - 1);
					if ('.' == stoplistMychar) {
						stoplistWord = stoplistWord.substring(0,
								stoplistWord.length() - 1);
					}

					stopWordList.add(stoplistWord);
				}
				tokens_sw.nextToken();
			}

			for (Iterator<String> i = fileWordList.iterator(); i.hasNext();) {
				String s = i.next();
				for (Iterator<String> j = stopWordList.iterator(); j.hasNext();) {

					if (j.next().equalsIgnoreCase(s)) {
						i.remove();
						break;
					}
				}

			}
			assetManager.close();
		} catch (IOException e) {
			e.printStackTrace();
			assetManager.close();
		}

	}

	/*********************************************************************
	 * Stemming of words using WordNet Access WordNet Dictionary to gives
	 * accurate stemming output
	 * 
	 * @throws IOException
	 *             , MalformedURLException
	 *********************************************************************/
	public void stemWords() throws IOException, MalformedURLException {

		try{
			
//		URL url = new URL("file", "/dict", ""); // this one load from my server

		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		URL url = classLoader.getResource("dict");
		IDictionary dict = new Dictionary(url);
		dict.open();
		/*
		 * This one load from SD card, if i am not mistaken, loading from my
		 * server or from SD card has same speed String path=
		 * Environment.getExternalStorageDirectory().getPath() + "/"+"dict"; URL
		 * url=new URL("file",null,path);
		 */
//		String path = Environment.getExternalStorageDirectory().getPath() + "/"
//				+ "dict";
//		URL url = new URL("file", null, path);

		WordnetStemmer stem = new WordnetStemmer(dict);
		for (int i = 0; i < fileWordList.size(); i++) {
			List<String> rs = stem.findStems(fileWordList.get(i), null);
			if (rs.size() > 0)
				fileWordList.set(i, rs.get(0));
		}
		}catch (MalformedURLException e){
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * POS Tagging 
	 * @throws IOException
	 * @throws Exception
	 ***************************************************************************/
	public void postagWords() throws IOException, Exception {
//		InputStreamReader modelIn = null;
		InputStream posMaxent;
		
//		String path = Environment.getExternalStorageDirectory().getPath() + "/"
//				+ "en-pos-maxent.bin";
			AssetManager assetManager = getAssets();

			try{
			posMaxent = assetManager.open("en-pos-maxent.bin");
			
			POSModel model = new POSModel(posMaxent);
			POSTaggerME tagger = new POSTaggerME(model);
			

		String[] fileWord = fileWordList.toArray(new String[fileWordList.size()]);

		// Tag the words stored in the array
		String[] tags = tagger.tag(fileWord);

		List<String> tagList = Arrays.asList(tags);
		taggedWordList = new ArrayList<String>(tagList);
		
		HashMap<String, String> hashTagged = new HashMap<String, String>();

		for (int i = 0; i < fileWordList.size(); i++) {
			if(hashTagged.get(fileWordList)==null){
				hashTagged.put(fileWordList.get(i), taggedWordList.get(i));
			}
			System.out.println("Word:" + fileWordList.get(i) + 
							" Tagged: " + taggedWordList.get(i));
		} 
			}catch (Exception e){
				e.printStackTrace();
			}
	}

}
