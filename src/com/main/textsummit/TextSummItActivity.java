package com.main.textsummit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class TextSummItActivity extends Activity {

	String LOGTAG = "EVENT";
	int REQUEST_CODE_SUMMARIZE_TEXT = 2;

	// BrowseFile
	int REQUEST_CODE_PICK_FILE = 1;
	int REQUEST_CODE_PICK_REF_FILE = 2;
	String newFile;
	String refFile;
	String selected_filename;
	String selected_refFile;

	// Layout Objects
	EditText et_file;
	EditText et_refFile;
	Button summarize;

	OnClickListener listener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.textsummit);

		et_file = (EditText) findViewById(R.id.edit_file_selected);
		et_refFile = (EditText) findViewById(R.id.edit_file_selected2);
		summarize = (Button) findViewById(R.id.btnSummarize);

		et_file.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startBrowseFile();
			}
		});

		et_refFile.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startBrowseRefFile();
			}
		});

		summarize.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				// Dialog box to alert user when input file is empty
				AlertDialog alertDialog = new AlertDialog.Builder(
						TextSummItActivity.this).create();

				String str_fn = et_file.getText().toString().trim();
				String str_refFn = et_refFile.getText().toString().trim();

				if (str_fn.equals("") || str_refFn.equals("")) {
					alertDialog.setTitle("Error Message");
					alertDialog
							.setMessage("Please select input file and reference file.");

					alertDialog.setButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									// close dialog box
									dialog.cancel();
								}
							});
					alertDialog.show();
				} else
					startSummarize();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// MenuInflater menuInflater = getMenuInflater();
		getMenuInflater().inflate(R.menu.menusummary, menu);
		return true;
	}

	/***************************************************************
	 * Browse file directory intent Files are load from sdcard
	 ***************************************************************/
	public void startBrowseFile() {
		Activity activityForBrowse = this;

		Intent fileExploreIntent = new Intent(
				com.main.textsummit.FileBrowserActivity.INTENT_ACTION_SELECT_FILE,
				null, activityForBrowse,
				com.main.textsummit.FileBrowserActivity.class);

		fileExploreIntent
				.putExtra(
						com.main.textsummit.FileBrowserActivity.showCannotReadParameter,
						false);
		startActivityForResult(fileExploreIntent, REQUEST_CODE_PICK_FILE);
	}

	public void startBrowseRefFile() {
		Activity activityForBrowse = this;

		Intent fileExploreIntent = new Intent(
				com.main.textsummit.FileBrowserActivity.INTENT_ACTION_SELECT_FILE,
				null, activityForBrowse,
				com.main.textsummit.FileBrowserActivity.class);

		fileExploreIntent
				.putExtra(
						com.main.textsummit.FileBrowserActivity.showCannotReadParameter,
						false);
		startActivityForResult(fileExploreIntent, REQUEST_CODE_PICK_REF_FILE);
	}

	/****************************************************************
	 * Summarization intent Selected file path will be send to
	 * SummarizeTextActivity
	 ****************************************************************/
	public void startSummarize() {
		final Activity activityForSummary = this;

		Log.d(LOGTAG, "Summarize Text button pressed");

		Intent summarizeIntent = new Intent(
				com.main.textsummit.SummarizeTextActivity.INTENT_ACTION_SUMMARIZE_TEXT,
				null, activityForSummary,
				com.main.textsummit.SummarizeTextActivity.class);

		selected_filename = newFile;
		selected_refFile = refFile;

		summarizeIntent.putExtra("SelectedFilePath", selected_filename);
		summarizeIntent.putExtra("SelectedRefFilePath", selected_refFile);
		startActivityForResult(summarizeIntent, REQUEST_CODE_SUMMARIZE_TEXT);
	}

	/****************************************************************
	 * Results from browseFile activity
	 ****************************************************************/
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == REQUEST_CODE_PICK_FILE) {
			if (resultCode == this.RESULT_OK) {
				newFile = data
						.getStringExtra(com.main.textsummit.FileBrowserActivity.returnFileParameter);
				Toast.makeText(this,
						"Selected FILE from file browser:\n" + newFile,
						Toast.LENGTH_SHORT).show();

				// Output selected file
				et_file = (EditText) findViewById(R.id.edit_file_selected);
				et_file.setText(newFile);

			} else {
				Toast.makeText(this, "NO result from file browser",
						Toast.LENGTH_SHORT).show();
			}
		}

		if (requestCode == REQUEST_CODE_PICK_REF_FILE) {
			if (resultCode == this.RESULT_OK) {
				refFile = data
						.getStringExtra(com.main.textsummit.FileBrowserActivity.returnFileParameter);
				Toast.makeText(this,
						"Selected REF FILE from file browser:\n" + refFile,
						Toast.LENGTH_SHORT).show();

				// Output selected file
				et_refFile = (EditText) findViewById(R.id.edit_file_selected2);
				et_refFile.setText(refFile);

			} else {
				Toast.makeText(this, "NO result from file browser",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

}
