package com.main.textsummit;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.TreeMap;

import android.app.Activity;
import android.content.res.AssetManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Environment;
import android.util.Log;

public class KeyWordProcess extends Activity{

	protected ArrayList<String> addKeyWordsToFile(Word[] addKeyWords,int wordCount, double percent) throws IOException {

		// Keyword is arranged according to highest frequency to lowest
		ArrayList<String> keyWordList = new ArrayList<String>();
		for (int i = 0; i < addKeyWords.length; i++) {
			for (int j = i + 1; j < addKeyWords.length; j++) {
				if ((double) addKeyWords[i].getFreq() / (double) wordCount < (double) addKeyWords[j]
						.getFreq() / (double) wordCount) {
					Word tmp = addKeyWords[i];
					addKeyWords[i] = addKeyWords[j];
					addKeyWords[j] = tmp;
				}
			}
		}

		for (int i = 0; 100 * i <= addKeyWords.length * percent * 100; i++) {
			keyWordList.add(addKeyWords[i].getName());
		}

		Collections.sort(keyWordList);
		
		///////// File input/output to keywords.txt ///////
		File myFile = new File(Environment.getExternalStorageDirectory().getPath() + "/keywords.txt");

		if(!myFile.exists()){
			try{
				myFile.createNewFile();
			}catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//true = append file
		FileWriter fileWritter = new FileWriter(myFile,true);
        BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
        bufferWritter.write(keyWordList.toString());
        bufferWritter.close();
        
		return keyWordList;
	}
}
