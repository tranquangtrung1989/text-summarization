package com.main.textsummit;

public class LCS{
    public LCS(String a, String b) {
        this.a = a;
        this.b = b;

        initDp();
    }

    // strings a, b
    private String a;
    private String b;

    // ls(i,j) - maximum length of common subsequence that end at a[i] and b[j].
    private int ls[][];

    // specifies which neighboring cell ls(i,j) it got its value
    Direction direction[][];

    private void initDp() {
        // init with 0 by default
        ls = new int[a.length() + 1][b.length() + 1];

        // so this could be skipped
        for (int i = 0; i <= a.length(); i++)
            ls[i][0] = 0;
        for (int j = 0; j <= b.length(); j++)
            ls[0][j] = 0;

        direction = new Direction[a.length() + 1][b.length() + 1];

        for (int i = 1; i <= a.length(); i++) {
            for (int j = 1; j <= b.length(); j++) {
                if (a.charAt(i - 1) == b.charAt(j - 1)) {
                    ls[i][j] = ls[i - 1][j - 1] + 1;
                    direction[i][j] = Direction.DIAG;
                } else if (ls[i - 1][j] >= ls[i][j - 1]) {
                    ls[i][j] = ls[i - 1][j];
                    direction[i][j] = Direction.UP;
                } else {
                    ls[i][j] = ls[i][j - 1];
                    direction[i][j] = Direction.LEFT;
                }
            }
        }
    }

    public void printLs() {
        System.out.printf("%1$2c ", ' ');
        for (int j = 0; j <= b.length(); j++)
            System.out.printf("%1$2c ", (j == 0) ? ' ' : b.charAt(j - 1));
        System.out.printf("\n");

        for (int i = 0; i <= a.length(); i++) {
            System.out.printf("%1$2c ", (i == 0) ? ' ' : a.charAt(i - 1));

            for (int j = 0; j <= b.length(); j++)
                System.out.printf("%1$2d ", ls[i][j]);
            System.out.printf("\n");
        }
    }

    public void printDirection() {
        System.out.printf("%1$2c ", ' ');
        for (int j = 0; j <= b.length(); j++)
            System.out.printf("%1$2c ", (j == 0) ? ' ' : b.charAt(j - 1));
        System.out.printf("\n");

        for (int i = 0; i <= a.length(); i++) {
            System.out.printf("%1$2c ", (i == 0) ? ' ' : a.charAt(i - 1));

            for (int j = 0; j <= b.length(); j++)
                System.out.printf("%1$2s ", (direction[i][j] == null)? '.' : direction[i][j]);
            System.out.printf("\n");
        }
    }

    public String getLcs() {
        StringBuilder sb = new StringBuilder();

        int i = a.length();
        int j = b.length();
        while (i > 0 && j > 0) {
            if (direction[i][j] == Direction.DIAG) {
                sb.append(a.charAt(i - 1));
                i--;
                j--;
            } else if (direction[i][j] == Direction.UP)
                i--;
            else
                j--;
        }
        return sb.reverse().toString();
    }

    private enum Direction {
        LEFT, UP, DIAG;

        @Override
        public String toString() {
            switch (this) {
                case LEFT:
                    return "<";
                case UP:
                    return "^";
                case DIAG:
                    return "\\";
            }
            throw new IllegalStateException("wrong direction");
        }
    }


}
