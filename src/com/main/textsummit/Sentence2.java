package com.main.textsummit;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import android.os.Environment;

import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;
import edu.mit.jwi.item.Synset;

public class Sentence2 {

	// private final static Log LOGTR = LogFactory.getLog(TextRankScore.class
	// .getName());

	/**
	 * @param args
	 */
	private double score;
	private String sentence; // the string sentence
	private String[] sentenceWords; // breaking sentence into array of string
	private ArrayList<String> wordArrayList;
	
	// consisted of words
	private int totalWords; // how many words each sentence has
	private String sentenceKeywords2;
	
	private String fname;
	private String title;
	private String firstSentence;
	private String lastSentence;
	private double senFirst, senLast;
	private double firstFeature, secondFeature, thirdFeature, fourthFeature, fifthFeature;
	
	public Sentence2() {
	}

	public Sentence2(String sentnc) {
		this.sentence = sentnc;
	}

	public int compareTo(Object o) { // used in sorting the sentence based on
										// weight
		Sentence2 p = (Sentence2) o;

		if (this.score < p.score) {
			return 1;
		} else if (this.score > p.score) {
			return -1;
		}
		return 0;
	}

	public void setSentenceWords() {
		String lowerSentence = sentence.toLowerCase();
		sentenceWords = lowerSentence.split(" ");
		totalWords = sentenceWords.length;
	}

	/***************************************
	 ****** Sentence features score ********
	 ***************************************/

	/*----------- TextRank score ----------*/
	public void textRank() throws Exception {
		
	}

	/*--------- Title feature score ----------*/
	public void compareWithTitle() {
		String lowerTitle = title.toLowerCase();
		String[] title1 = lowerTitle.split(" ");
		double titleWords = title1.length;

		int i, j;
		int same = 0;

		for (i = 0; i < titleWords; i++) {
			for (j = 0; j < totalWords; j++) {
				if (title1[i].equals(sentenceWords[j])) {
					same++;
				}
			}
		}
		secondFeature = same / titleWords;
		// System.out.println(firstFeature+"");
	}

	/*------------ Position score --------------*/
	public void compareWithFirst() {
		String lowerFirst = firstSentence.toLowerCase();
		String[] first = lowerFirst.split(" ");
		int firstWords = first.length;

		int i, j;
		int same = 0;

		for (i = 0; i < firstWords; i++) {
			for (j = 0; j < totalWords; j++) {
				if (first[i].equals(sentenceWords[j]))
					same++;
			}
		}

		senFirst = same / firstWords;

	}

	public void compareWithLast() {
		String lowerLast = lastSentence.toLowerCase();
		String[] last = lowerLast.split(" ");
		int lastWords = last.length;

		int i, j;
		int same = 0;

		for (i = 0; i < lastWords; i++) {
			for (j = 0; j < totalWords; j++) {
				if (last[i].equals(sentenceWords[j]))
					same++;
			}
		}

		senLast = same / lastWords;
	}

	public void averageSFL() {
		thirdFeature = (senFirst + senLast) / 2;
	}
	
	public void secPosModel(TreeMap<String, Integer> mapSen, int totalSen){
		
		int dent = 2; //Dent factor
		int senPos = (Integer) mapSen.get(sentence);
		
		fourthFeature = ( Math.cos((2*Math.PI*senPos)/(totalSen-1)) + dent - 1 ) / dent;
//		System.out.println(fourthFeature);
	}

	/*------------ WordNet score ---------------*/
	public void wordNet(ArrayList<String> keyWordList) throws Throwable {
		double score, wnScore;
		double totalScore=0;
		int lvl = 0;
		
		//if words exist in same synset then increase lvl by 1
		for (int i=0; i<sentenceWords.length; i++){
			List<String> wordList = Arrays.asList(sentenceWords);
			wordArrayList = new ArrayList<String>(wordList);
		}
		
		String path = Environment.getExternalStorageDirectory().getPath() + "/" + "dict";
		URL url = new URL("file", null, path);
		IDictionary dict = new Dictionary(url);
		dict.open();
		
//		for(int i=0; i<keyWordList.size(); i++){
//			IIndexWord idxWord = dict.getIndexWord(keyWordList.get(i), POS.NOUN);
//			IWordID wordID = idxWord.getWordIDs().get(0);
//			IWord word = dict.getWord(wordID);
//			ISynset synset = word.getSynset();
//			
//			System.out.println(synset.getWords());
//		}
		
		for (Iterator<String> i = wordArrayList.iterator(); i.hasNext();) {
			
			for (Iterator<String> j = keyWordList.iterator(); j.hasNext();) {
				String s = j.next();
				if (i.next().contains(s)) {
					
					break;
				}
			}

		}
		score = 1 / Math.pow(2, lvl);
		totalScore += score;
		
		wnScore = totalScore; 
	}

	/*------------ Sentence similarity score ---------------*/

	public void sentenceSimilarity(int totalKeywords) {
		String[] keyWord = sentenceKeywords2.split(",");
		fifthFeature = keyWord.length / totalKeywords;
	}

	/*----------Total Sentence score ---------------*/
	public double totalSentenceScore() {
		score = secondFeature + fourthFeature + fifthFeature;
		return score;
	}

	/******************************
	 ****** Get/Set methods *******
	 ******************************/
	public void setSelectedFile(String filename) {
		this.fname = filename;
	}

	public void setSentence(String ssentnc) {
		this.sentence = ssentnc;
	}

	public void setTitle(String ttl) {
		this.title = ttl;
	}

	public void setFirstSentence(String fs) {
		this.firstSentence = fs;
	}

	public void setLastSentence(String ls) {
		this.lastSentence = ls;
	}

	public String returnSentence() {
		return (sentence);
	}

	public String getSentenceKeywords() {
		return sentenceKeywords2;
	}

	public void setSentenceKeywords(Word[] words_freq, int wordListLength) {
		String tmpX;
		List disKey = new ArrayList();
		String[] disKey2;
		int y;
		for (y = 0; y < totalWords; y++) {
			for (int x = 0; x < wordListLength; x++) {
				if (sentenceWords[y].equals(words_freq[x].getName())) {
					if (words_freq[x].getKeyProb() > 0.0) {
						tmpX = words_freq[x].getName();
						disKey.add(tmpX);
					}
				}
			}
		}

		disKey2 = new String[disKey.size()];

		for (y = 0; y < disKey.size(); y++) {
			disKey2[y] = (String) disKey.get(y);
		}

		String tmp2, tmpKeywords = "";

		for (y = 0; y < disKey.size(); y++) {
			tmp2 = disKey2[y];
			tmpKeywords = tmp2 + ", " + tmpKeywords;
		}

		this.sentenceKeywords2 = tmpKeywords;
	}
}
