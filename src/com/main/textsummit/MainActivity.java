package com.main.textsummit;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.AdapterView.OnItemClickListener;

public class MainActivity extends Activity {

	String LOGTAG = "EVENT";
	
	// Layout Objects
	GridView homeGrid;
	
	static final String[] home_icon = new String[] { 
		"TextSummIt", "Favorites", "Settings", "Help" };

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		homeGrid = (GridView) findViewById(R.id.gridView1);
		homeGrid.setAdapter(new ImageAdapter(this, home_icon));
		
		homeGrid.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				switch(position){
				case 0:
					openTextSummIt();
					break;
				case 1:
					openFavorites();
					break;
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	public void openTextSummIt(){
    	Log.d(LOGTAG, "Grid: Open TextSummIt");
    	Intent i = new Intent(MainActivity.this, TextSummItActivity.class);
		startActivity(i);
    }
    
    public void openFavorites(){
    	Log.d(LOGTAG, "Grid: Open Favorites");
//    	Intent i = new Intent(MainActivity.this, FavoritesActivity.class);
//		startActivity(i);
    }
    
}
